module.exports.iniciaChat = (app, req, res) => {
  const { body } = req

  req.assert('apelido', 'Apelido é obrigatório.').notEmpty()
  req.assert('apelido', 'Apelido deve conter entre 3 e 15 caracteres.').len(3, 15)

  const errors = req.validationErrors()

  if (errors) {
    res.render('index', { errors })
  } else {
    app.get('io').emit(
      'msgParaCliente',
      {
        apelido: body.apelido,
        mensagem: `${body.apelido} acabou de entrar no chat`
      }
    )

    res.render('chat', { body })
  }
}
