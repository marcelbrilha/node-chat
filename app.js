const app = require('./config/server')

const server = app.listen(process.env.CHAT_PORT, () => {
  console.log(`Running in port ${process.env.CHAT_PORT}`)
})

const socketIO = require('socket.io').listen(server)

app.set('io', socketIO)

socketIO.on('connection', (socket) => {
  console.log('Usuário conectou')

  socket.on('disconnect', () => {
    console.log('Usuário desconectou')
  })

  socket.on('msgParaServidor', (data) => {
    // dialogo
    socket.emit('msgParaCliente', data)
    socket.broadcast.emit('msgParaCliente', data)

    // participantes
    if (parseInt(data.apelidoAtualizado) === 0) {
      socket.emit('participantesParaCliente', data)
      socket.broadcast.emit('participantesParaCliente', data)
    }
  })
})
